TARGET = LavaTulipGl
TEMPLATE = app

QT += widgets opengl
LIBS += -lGLU

SOURCES += \
    main.cpp \
    lavatulipwindow.cpp \
    glview.cpp \
    viewstate.cpp \
    geometrybuilder.cpp

HEADERS += \
    glview.h \
    lavatulipwindow.h \
    viewstate.h \
    geometrybuilder.h

FORMS += \
    lavatulipwindow.ui
