#ifndef LAVATULIPWINDOW_H
#define LAVATULIPWINDOW_H

#include <QList>
#include <QWidget>

#include "geometrybuilder.h"

namespace Ui {
class LavaTulipWindow;
}

class LavaTulipWindow : public QWidget
{
    Q_OBJECT
public:
    explicit LavaTulipWindow(QWidget *parent = 0);
    ~LavaTulipWindow();

private slots:
    void onModelToggled(bool enabled);
    void onWireFrameToggled(bool enabled);
    void onNormalMapToggled(bool enabled);

    void onPerspectiveClicked();
    void onXyClicked();
    void onXzClicked();
    void onYzClicked();

    void onCreateClicked();

private:
    Ui::LavaTulipWindow *ui;

    GeometryBuilder m_builder;
};

#endif // LAVATULIPWINDOW_H
