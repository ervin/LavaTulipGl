#include "geometrybuilder.h"

#include <cmath>

#include "glview.h"

GeometryBuilder::GeometryBuilder()
{
}

void GeometryBuilder::build(GeometryType type, float x, float y, float z)
{
    m_commands << BuildCommand{type, x, y, z};

    QVector<ListIds> idsVector;

    foreach (const BuildCommand &command, m_commands) {
        switch (command.type) {
        case BarGeometry:
            idsVector << buildBar(command.x, command.y, command.z);
            break;
        case RingGeometry:
            idsVector << buildRing(command.x, command.y, command.z);
            break;
        case WaveGeometry:
            idsVector << buildWave(command.x, command.y, command.z);
            break;
        case NoGeometry:
            qFatal("Should not happen");
        }
    }

    GlView *view = GlView::instance();

    view->beginList(ViewState::ModelList);
    foreach (const ListIds &ids, idsVector)
        view->callList(ids.model);
    view->endList();

    view->beginList(ViewState::WireFrameList);
    foreach (const ListIds &ids, idsVector)
        view->callList(ids.wireFrame);
    view->endList();

    view->beginList(ViewState::NormalMapList);
    foreach (const ListIds &ids, idsVector)
        view->callList(ids.normalMap);
    view->endList();

    view->update();
}

QVector<float> squareFacePoints(float centerX, float centerY, float centerZ,
                                float normalX, float normalZ, // normalY is always 0
                                float extent)
{
    float norm = std::sqrt(normalX * normalX + normalZ * normalZ);
    float normalizedX = normalX / norm;
    float normalizedZ = normalZ / norm;
    float extentX = extent * normalizedZ;
    float extentZ = extent * normalizedX;

    QVector<float> result;
    result << centerX - extentX << centerY + extent << centerZ - extentZ
           << centerX - extentX << centerY - extent << centerZ - extentZ
           << centerX + extentX << centerY - extent << centerZ + extentZ
           << centerX + extentX << centerY + extent << centerZ + extentZ;
    return result;
}

QVector<float> squareFaceNormals(float normalX, float normalZ) // normalY is always 0
{
    float norm = std::sqrt(normalX * normalX + normalZ * normalZ);
    float normalizedX = normalX / norm;
    float normalizedZ = normalZ / norm;

    QVector<float> result;
    result << -normalizedZ << 1.0f << -normalizedX
           << -normalizedZ << -1.0f << -normalizedX
           << normalizedZ << -1.0f << normalizedX
           << normalizedZ << 1.0f << normalizedX;
    return result;
}


GeometryBuilder::ListIds GeometryBuilder::buildBar(float x, float y, float z)
{
    GlView *view = GlView::instance();
    ListIds ids;

    float length = 1.5f;
    float extent = 0.1f;
    QVector<float> positions = squareFacePoints(x, y, z + length / 2.0f,
                                                0.0f, 1.0f,
                                                extent)
                             + squareFacePoints(x, y, z - length / 2.0f,
                                                0.0f, -1.0f,
                                                extent);

    QVector<float> normals = squareFaceNormals(0.0f, 1.0f)
                           + squareFaceNormals(0.0f, -1.0f);

    QVector<float> frontNormals;
    frontNormals << 0.0f << 0.0f << 1.0f
                 << 0.0f << 0.0f << 1.0f
                 << 0.0f << 0.0f << 1.0f
                 << 0.0f << 0.0f << 1.0f;

    QVector<float> backNormals;
    backNormals << 0.0f << 0.0f << -1.0f
                << 0.0f << 0.0f << -1.0f
                << 0.0f << 0.0f << -1.0f
                << 0.0f << 0.0f << -1.0f;

    QVector<float> colors;
    for (int i = 0; i < (positions.size() / 3); i++)
        colors << 1.0f << 0.0f << 0.0f << 1.0f;

    const int indices[4][5] = { { 0, 3, 4, 7, 0 }, { 2, 1, 6, 5, 2 }, { 3, 2, 5, 4, 3 }, { 7, 6, 1, 0, 7 } };
    QVector<float> polylinePositions;
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 5; j++) {
            polylinePositions << *(positions.data() + 3 * indices[i][j])
                              << *(positions.data() + 3 * indices[i][j] + 1)
                              << *(positions.data() + 3 * indices[i][j] + 2);
        }
    }

    QVector<float> polylineColors;
    for (int i = 0; i < 5; i++)
        polylineColors << 0.0f << 0.0f << 0.0f << 1.0f;

    ids.model = view->createList();
    view->addFaces(positions.data(), normals.data(), colors.data(), 8);
    view->addFaces(positions.data(), frontNormals.data(), colors.data(), 4);
    view->addFaces(positions.data() + 12, backNormals.data(), colors.data() + 16, 4);
    view->releaseList();

    for (int i = 0; i < (normals.size() / 3); i++) {
        for (int j = 0; j < 3; j++)
            colors[i * 4 + j] = (normals[i * 3 + j] + 1.0f) / 2.0f;
        colors[i * 4 + 3] = 0.75f;
    }

    ids.normalMap = view->createList();
    view->addFaces(positions.data(), normals.data(), colors.data(), 8);
    view->addFaces(positions.data(), normals.data(), colors.data(), 4);
    view->addFaces(positions.data() + 12, normals.data() + 12, colors.data() + 16, 4);
    view->releaseList();

    ids.wireFrame = view->createList();
    for (int i = 0; i < (polylinePositions.size() / 15); i++)
        view->addPolyline(polylinePositions.data() + i * 15, polylineColors.data(), 5);
    view->releaseList();

    return ids;
}

GeometryBuilder::ListIds GeometryBuilder::buildRing(float x, float y, float z)
{
    GlView *view = GlView::instance();
    ListIds ids;

    float extent = 0.2f;
    float radius = 2.0f;
    int stepCount = 64;

    QVector<float> positions;
    QVector<float> normals;

    for (int step = 0; step < stepCount; step++) {
        float stepX = radius * float(std::cos(step * 2 * M_PI / stepCount));
        float stepZ = radius * float(std::sin(step * 2 * M_PI / stepCount));

        positions += squareFacePoints(x + stepX, y, z + stepZ,
                                      stepZ, stepX,
                                      extent);
        normals += squareFaceNormals(stepZ, stepX);

        stepX = radius * float(std::cos((step + 1) * 2 * M_PI / stepCount));
        stepZ = radius * float(std::sin((step + 1) * 2 * M_PI / stepCount));

        positions += squareFacePoints(x + stepX, y, z + stepZ,
                                      -stepZ, -stepX,
                                      extent);
        normals += squareFaceNormals(-stepZ, -stepX);
    }

    QVector<float> colors;
    for (int i = 0; i < (positions.size() / 3); i++)
        colors << 1.0f << 0.0f << 0.0f << 1.0f;

    const int indices[4][5] = { { 0, 3, 4, 7, 0 }, { 2, 1, 6, 5, 2 }, { 3, 2, 5, 4, 3 }, { 7, 6, 1, 0, 7 } };
    QVector<float> polylinePositions;
    for (int k = 0; k < (positions.size() / 24); k++) {
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 5; j++) {
                polylinePositions << *(positions.data() + 24 * k + 3 * indices[i][j])
                                  << *(positions.data() + 24 * k + 3 * indices[i][j] + 1)
                                  << *(positions.data() + 24 * k + 3 * indices[i][j] + 2);
            }
        }
    }

    QVector<float> polylineColors;
    for (int i = 0; i < 5; i++)
        polylineColors << 0.0f << 0.0f << 0.0f << 1.0f;

    ids.model = view->createList();
    for (int k = 0; k < (positions.size() / 24); k++)
        view->addFaces(positions.data() + 24 * k, normals.data() + 24 * k, colors.data() + 32 * k, 8);
    view->releaseList();

    for (int i = 0; i < (normals.size() / 3); i++) {
        for (int j = 0; j < 3; j++)
            colors[i * 4 + j] = (normals[i * 3 + j] + 1.0f) / 2.0f;
        colors[i * 4 + 3] = 0.75f;
    }

    ids.normalMap = view->createList();
    for (int k = 0; k < (positions.size() / 24); k++)
        view->addFaces(positions.data() + 24 * k, normals.data() + 24 * k, colors.data() + 32 * k, 8);
    view->releaseList();

    ids.wireFrame = view->createList();
    for (int i = 0; i < (polylinePositions.size() / 15); i++)
        view->addPolyline(polylinePositions.data() + i * 15, polylineColors.data(), 5);
    view->releaseList();

    return ids;
}

GeometryBuilder::ListIds GeometryBuilder::buildWave(float x, float y, float z)
{
    GlView *view = GlView::instance();
    ListIds ids;

    float length = 3.5f;
    float radius = 0.5f;
    int rings = 32;
    int slices = 32;

    QVector<float> positions;
    QVector<float> normals;

    for (int ring = 0; ring < rings; ring++) {
        float ringZ = z - length / 2.0f + ring * length / float(rings - 1);
        float nextRingZ = ringZ + length / float(rings - 1);

        for (int slice = 0; slice <= slices; slice++) {
            float sliceAngle = float(slice) * 2.0f * float(M_PI) / float(slices);
            float sliceCos = std::cos(sliceAngle);
            float sliceSin = std::sin(sliceAngle);
            float sliceX = x + radius * sliceCos;
            float sliceY = y + radius * sliceSin;

            float nextSliceAngle = sliceAngle + 2.0f * float(M_PI) / float(slices);
            float nextSliceCos = std::cos(nextSliceAngle);
            float nextSliceSin = std::sin(nextSliceAngle);
            float nextSliceX = x + radius * nextSliceCos;
            float nextSliceY = y + radius * nextSliceSin;

            positions << sliceX << std::sin(2.0f * ringZ) + sliceY << ringZ
                      << nextSliceX << std::sin(2.0f * ringZ) + nextSliceY << ringZ
                      << sliceX << std::sin(2.0f * nextRingZ) + sliceY << nextRingZ
                      << nextSliceX << std::sin(2.0f * ringZ) + nextSliceY << ringZ
                      << nextSliceX << std::sin(2.0f * nextRingZ) + nextSliceY << nextRingZ
                      << sliceX << std::sin(2.0f * nextRingZ) + sliceY << nextRingZ;

            normals << sliceCos << sliceSin << 0.0f
                    << nextSliceCos << nextSliceSin << 0.0f
                    << sliceCos << sliceSin << 0.0f
                    << nextSliceCos << nextSliceSin << 0.0f
                    << nextSliceCos << nextSliceSin << 0.0f
                    << sliceCos << sliceSin << 0.0f;
        }
    }

    QVector<float> colors;
    for (int i = 0; i < (positions.size() / 3); i++)
        colors << 1.0f << 0.0f << 0.0f << 1.0f;

    QVector<float> polylinePositions;
    for (int i = 0; i < (positions.size() / 9); i++) {
        for (int j = 0; j < 3; j++) {
            polylinePositions << *(positions.data() + 9 * i + 3 * j)
                              << *(positions.data() + 9 * i + 3 * j + 1)
                              << *(positions.data() + 9 * i + 3 * j + 2);
        }
        polylinePositions << *(positions.data() + 9 * i)
                          << *(positions.data() + 9 * i + 1)
                          << *(positions.data() + 9 * i + 2);
    }

    QVector<float> polylineColors;
    for (int i = 0; i < 4; i++)
        polylineColors << 0.0f << 0.0f << 0.0f << 1.0f;

    ids.model = view->createList();
    for (int k = 0; k < (positions.size() / 9); k++)
        view->addFaces(positions.data() + 9 * k, normals.data() + 9 * k, colors.data() + 12 * k, 3);
    view->releaseList();

    for (int i = 0; i < (normals.size() / 3); i++) {
        for (int j = 0; j < 3; j++)
            colors[i * 4 + j] = (normals[i * 3 + j] + 1.0f) / 2.0f;
        colors[i * 4 + 3] = 0.75f;
    }

    ids.normalMap = view->createList();
    for (int k = 0; k < (positions.size() / 9); k++)
        view->addFaces(positions.data() + 9 * k, normals.data() + 9 * k, colors.data() + 12 * k, 3);
    view->releaseList();

    ids.wireFrame = view->createList();
    for (int i = 0; i < (polylinePositions.size() / 12); i++)
        view->addPolyline(polylinePositions.data() + i * 12, polylineColors.data(), 4);
    view->releaseList();

    return ids;
}
