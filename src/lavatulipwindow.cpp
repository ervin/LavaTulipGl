#include "lavatulipwindow.h"
#include "ui_lavatulipwindow.h"

LavaTulipWindow::LavaTulipWindow(QWidget *parent)
    : QWidget(parent),
      ui(new Ui::LavaTulipWindow)
{
    ui->setupUi(this);

    connect(ui->modelCheck, &QCheckBox::toggled, this, &LavaTulipWindow::onModelToggled);
    connect(ui->wireframeCheck, &QCheckBox::toggled, this, &LavaTulipWindow::onWireFrameToggled);
    connect(ui->normalMapCheck, &QCheckBox::toggled, this, &LavaTulipWindow::onNormalMapToggled);

    connect(ui->perspectiveButton, &QPushButton::clicked, this, &LavaTulipWindow::onPerspectiveClicked);
    connect(ui->xyButton, &QPushButton::clicked, this, &LavaTulipWindow::onXyClicked);
    connect(ui->xzButton, &QPushButton::clicked, this, &LavaTulipWindow::onXzClicked);
    connect(ui->yzButton, &QPushButton::clicked, this, &LavaTulipWindow::onYzClicked);

    connect(ui->createButton, &QPushButton::clicked, this, &LavaTulipWindow::onCreateClicked);

    ui->cameraZSpin->setValue(ui->glView->cameraZ());
    connect(ui->cameraZSpin, static_cast<void (QDoubleSpinBox::*)(double)>(&QDoubleSpinBox::valueChanged),
            ui->glView, &GlView::setCameraZ);
    connect(ui->glView, &GlView::cameraZChanged, ui->cameraZSpin, &QDoubleSpinBox::setValue);
}

LavaTulipWindow::~LavaTulipWindow()
{
    delete ui;
}

void LavaTulipWindow::onModelToggled(bool enabled)
{
    ui->glView->state().setEnabled(ViewState::ModelList, enabled);
    ui->glView->update();
}

void LavaTulipWindow::onWireFrameToggled(bool enabled)
{
    ui->glView->state().setEnabled(ViewState::WireFrameList, enabled);
    ui->glView->update();
}

void LavaTulipWindow::onNormalMapToggled(bool enabled)
{
    ui->glView->state().setEnabled(ViewState::NormalMapList, enabled);
    ui->glView->update();
}

void LavaTulipWindow::onPerspectiveClicked()
{
    ui->glView->setModelRotationX(45.0);
    ui->glView->setModelRotationY(45.0);
}

void LavaTulipWindow::onXyClicked()
{
    ui->glView->setModelRotationX(0.0);
    ui->glView->setModelRotationY(0.0);
}

void LavaTulipWindow::onXzClicked()
{
    ui->glView->setModelRotationX(90.0);
    ui->glView->setModelRotationY(0.0);
}

void LavaTulipWindow::onYzClicked()
{
    ui->glView->setModelRotationX(0.0);
    ui->glView->setModelRotationY(90.0);
}

void LavaTulipWindow::onCreateClicked()
{
    GeometryType type = NoGeometry;
    if (ui->primitiveTypeCombo->currentText() == "Bar")
        type = BarGeometry;
    else if (ui->primitiveTypeCombo->currentText() == "Ring")
        type = RingGeometry;
    else if (ui->primitiveTypeCombo->currentText() == "Wave")
        type = WaveGeometry;
    else
        qFatal("Should not happen");

    m_builder.build(type,
                    ui->xSpin->value(),
                    ui->ySpin->value(),
                    ui->zSpin->value());
}
