#ifndef GLVIEW_H
#define GLVIEW_H

#include <QGLWidget>

#include "viewstate.h"

class GlView : public QGLWidget
{
    Q_OBJECT
public:
    static GlView *instance();

    explicit GlView(QWidget *parent = Q_NULLPTR);
    ~GlView();

    ViewState &state();

    unsigned int createList();
    void releaseList();
    void callList(unsigned int id);

    ViewState::ListType currentList() const;

    void beginList(ViewState::ListType type);
    void endList();

    void setColor(float *color);
    void addFaces(float *position, float *normal, float *color, int count);
    void addPolyline(float *position, float *color, int count);

    qreal cameraZ() const;
    qreal modelRotationX() const;
    qreal modelRotationY() const;

public slots:
    void setCameraZ(qreal z);
    void setModelRotationX(qreal angle);
    void setModelRotationY(qreal angle);

signals:
    void cameraZChanged(qreal z);

protected:
    void initializeGL() Q_DECL_OVERRIDE;
    void resizeGL(int width, int height) Q_DECL_OVERRIDE;
    void paintGL() Q_DECL_OVERRIDE;

    void mousePressEvent(QMouseEvent *event) Q_DECL_OVERRIDE;
    void mouseReleaseEvent(QMouseEvent *event) Q_DECL_OVERRIDE;
    void mouseMoveEvent(QMouseEvent *event) Q_DECL_OVERRIDE;
    void wheelEvent(QWheelEvent *event) Q_DECL_OVERRIDE;

private:
    void applyRotation(const QPointF &start, const QPointF &current);

    qreal m_cameraZ;
    qreal m_modelRotationX;
    qreal m_modelRotationY;

    bool m_pressed;
    QPointF m_lastPoint;

    ViewState m_state;
    ViewState::ListType m_currentList;

    static GlView *s_instance;
};

#endif // GLVIEW_H
