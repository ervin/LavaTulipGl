#include <QApplication>

#include "lavatulipwindow.h"

int main(int argc, char **argv)
{
    QApplication app(argc, argv);

    LavaTulipWindow window;
    window.resize(1024, 700);
    window.show();

    return app.exec();
}
