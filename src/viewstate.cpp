#include "viewstate.h"

ViewState::ViewState()
    : m_modelId(0),
      m_wireFrameId(0),
      m_normalMapId(0),
      m_modelEnabled(true),
      m_wireFrameEnabled(true),
      m_normalMapEnabled(false)
{
}

unsigned int ViewState::modelId() const
{
    return m_modelId;
}

void ViewState::setModelId(unsigned int id)
{
    m_modelId = id;
}

unsigned int ViewState::wireFrameId() const
{
    return m_wireFrameId;
}

void ViewState::setWireFrameId(unsigned int id)
{
    m_wireFrameId = id;
}

unsigned int ViewState::normalMapId() const
{
    return m_normalMapId;
}

void ViewState::setNormalMapId(unsigned int id)
{
    m_normalMapId = id;
}

bool ViewState::isEnabled(ViewState::ListType type) const
{
    switch (type) {
    case ModelList:
        return m_modelEnabled;
    case WireFrameList:
        return m_wireFrameEnabled;
    case NormalMapList:
        return m_normalMapEnabled;
    case TempList:
        return false;
    case NoList:
        return false;
    }
}

void ViewState::setEnabled(ViewState::ListType type, bool enabled)
{
    switch (type) {
    case ModelList:
        m_modelEnabled = enabled;
        break;
    case WireFrameList:
        m_wireFrameEnabled = enabled;
        break;
    case NormalMapList:
        m_normalMapEnabled = enabled;
        break;
    case TempList:
        break;
    case NoList:
        break;
    }
}
