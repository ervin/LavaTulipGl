#ifndef GEOMETRYBUILDER_H
#define GEOMETRYBUILDER_H

#include <QVector>

enum GeometryType
{
    NoGeometry = 0,
    BarGeometry,
    RingGeometry,
    WaveGeometry
};

class GeometryBuilder
{
public:
    GeometryBuilder();

    void build(GeometryType type, float x, float y, float z);

private:
    struct BuildCommand {
        GeometryType type;
        float x;
        float y;
        float z;
    };

    struct ListIds {
        unsigned int model;
        unsigned int wireFrame;
        unsigned int normalMap;

        ListIds() : model(0), wireFrame(0), normalMap(0) {}
    };

    ListIds buildBar(float x, float y, float z);
    ListIds buildRing(float x, float y, float z);
    ListIds buildWave(float x, float y, float z);

    QVector<BuildCommand> m_commands;
};

#endif // GEOMETRYBUILDER_H
