#ifndef VIEWSTATE_H
#define VIEWSTATE_H

class ViewState
{
public:
    enum ListType {
        NoList = 0,
        ModelList,
        WireFrameList,
        NormalMapList,
        TempList
    };

    ViewState();

    unsigned int modelId() const;
    void setModelId(unsigned int id);

    unsigned int wireFrameId() const;
    void setWireFrameId(unsigned int id);

    unsigned int normalMapId() const;
    void setNormalMapId(unsigned int id);

    bool isEnabled(ListType type) const;
    void setEnabled(ListType type, bool enabled);

private:
    unsigned int m_modelId;
    unsigned int m_wireFrameId;
    unsigned int m_normalMapId;

    bool m_modelEnabled;
    bool m_wireFrameEnabled;
    bool m_normalMapEnabled;
};

#endif // VIEWSTATE_H
