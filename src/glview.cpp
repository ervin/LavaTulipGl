#include "glview.h"

#include <GL/glu.h>

#include <QMouseEvent>
#include <QWheelEvent>

GlView *GlView::s_instance = Q_NULLPTR;

GlView *GlView::instance()
{
    return s_instance;
}

GlView::GlView(QWidget *parent)
    : QGLWidget(parent),
      m_cameraZ(-10.0),
      m_modelRotationX(45.0),
      m_modelRotationY(45.0),
      m_pressed(false),
      m_currentList(ViewState::NoList)
{
    Q_ASSERT(!s_instance);
    s_instance = this;
}

GlView::~GlView()
{
    s_instance = Q_NULLPTR;
}

ViewState &GlView::state()
{
    return m_state;
}

unsigned int GlView::createList()
{
    unsigned int id = glGenLists(1);
    glNewList(id, GL_COMPILE);
    m_currentList = ViewState::TempList;
    return id;
}

void GlView::callList(unsigned int id)
{
    glCallList(id);
}

void GlView::releaseList()
{
    m_currentList = ViewState::NoList;
    glEndList();
}

ViewState::ListType GlView::currentList() const
{
    return m_currentList;
}

void GlView::beginList(ViewState::ListType type)
{
    bool normalmap = false;

    if (m_currentList)
        return;

    context()->makeCurrent();

    switch (type) {
    case ViewState::ModelList:
        if (m_state.modelId())
            glDeleteLists(m_state.modelId(), 1);
        m_state.setModelId(glGenLists(1));
        glNewList(m_state.modelId(), GL_COMPILE);
        glEnable(GL_NORMALIZE);
        glEnable(GL_COLOR_MATERIAL);
        m_currentList = ViewState::ModelList;
        break;
    case ViewState::WireFrameList:
        if (m_state.wireFrameId())
            glDeleteLists(m_state.wireFrameId(), 1);
        m_state.setWireFrameId(glGenLists(1));
        glNewList(m_state.wireFrameId(), GL_COMPILE);
        glEnable(GL_COLOR_MATERIAL);
        m_currentList = ViewState::WireFrameList;
        break;
    case ViewState::NormalMapList:
        if (m_state.normalMapId())
            glDeleteLists(m_state.normalMapId(), 1);
        m_state.setNormalMapId(glGenLists(1));
        glNewList(m_state.normalMapId(), GL_COMPILE);
        normalmap = true;
        glEnable(GL_COLOR_MATERIAL);
        glEnable(GL_BLEND);
        m_currentList = ViewState::NormalMapList;
        break;
    case ViewState::TempList:
    case ViewState::NoList:
        qFatal("Should not happen");
    }

    if (normalmap) {
        float diffuse[] = {1.0f, 1.0f, 1.0f, 0.5f};
        float ambient[] = {0.1f, 0.1f, 0.1f, 0.5f};
        float specular[] = {0.0f, 0.0f, 0.0f, 1.0f};
        float shininess[] = {0.0f};

        glMaterialfv(GL_FRONT, GL_DIFFUSE, diffuse);
        glMaterialfv(GL_FRONT, GL_AMBIENT, ambient);
        glMaterialfv(GL_FRONT, GL_SPECULAR, specular);
        glMaterialfv(GL_FRONT, GL_SHININESS, shininess);
    } else {
        float diffuse[] = {0.75f, 0.75f, 0.75f, 1.0f};
        float ambient[] = {0.2f, 0.2f, 0.2f, 1.0f};
        float specular[] = {0.2f, 0.2f, 0.2f, 1.0f};
        float shininess[] = {150.0f};

        glMaterialfv(GL_FRONT, GL_DIFFUSE, diffuse);
        glMaterialfv(GL_FRONT, GL_AMBIENT, ambient);
        glMaterialfv(GL_FRONT, GL_SPECULAR, specular);
        glMaterialfv(GL_FRONT, GL_SHININESS, shininess);
    }
}

void GlView::endList()
{
    if (!m_currentList)
        return;

    context()->makeCurrent();

    glEndList();
    m_currentList = ViewState::NoList;
}

void GlView::setColor(float *color)
{
    context()->makeCurrent();
    glColor4fv(color);
}

void GlView::addFaces(float *position, float *normal, float *color, int count)
{
    float xyz[3];
    float nxyz[3];
    float rgba[4];
    context()->makeCurrent();
    const int n4[2][3] = { { 0, 1, 3 }, { 1, 2, 3 } };
    const int n8[4][4] = { { 0, 3, 4, 7 }, { 2, 1, 6, 5 }, { 3, 2, 5, 4 }, { 7, 6, 1, 0 } };

    switch (count) {
    case 3:
        glBegin(GL_TRIANGLES);
        for (int i = 0; i < 3; i++) {
            for (int k = 0; k < 4; k++)
                rgba[k] = color[4 * i + k];
            setColor(rgba);
            for (int k = 0; k < 3; k++)
                nxyz[k] = normal[3 * i + k];
            glNormal3fv(nxyz);
            for (int k = 0; k < 3; k++)
                xyz[k] = position[3 * i + k];
            glVertex3fv(xyz);
        }
        glEnd();
        break;
    case 4:
        glBegin(GL_TRIANGLES);
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 3; j++) {
                for (int k = 0; k < 4; k++)
                    rgba[k] = color[4 * n4[i][j] + k];
                setColor(rgba);
                for (int k = 0; k < 3; k++)
                    nxyz[k] = normal[3 * n4[i][j] + k];
                glNormal3fv(nxyz);
                for (int k = 0; k < 3; k++)
                    xyz[k] = position[3 * n4[i][j] + k];
                glVertex3fv(xyz);
            }
        }
        glEnd();
        break;
    case 8:
        glBegin(GL_QUADS);
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                for (int k = 0; k < 4; k++)
                    rgba[k] = color[4 * n8[i][j] + k];
                setColor(rgba);
                for (int k = 0; k < 3; k++)
                    nxyz[k] = normal[3 * n8[i][j] + k];
                glNormal3fv(nxyz);
                for (int k = 0; k < 3; k++)
                    xyz[k] = position[3 * n8[i][j] + k];
                glVertex3fv(xyz);
            }
        }
        glEnd();
        break;
    default:
        qFatal("Should not happen");
    }
}

void GlView::addPolyline(float *position, float *color, int count)
{
    float xyz[3];
    float rgba[4];
    context()->makeCurrent();

    glBegin(GL_LINE_STRIP);
    for (int i = 0; i < count; i++) {
        for (int j = 0; j < 4; j++) {
            rgba[j] = *(color + j);
        }
        setColor(rgba);
        for (int j = 0; j < 3; j++) {
            xyz[j] = *(position + j);
        }

        if (i > 0) {
            glNormal3f(*position - *(position - 3),
                       *(position + 1) - *(position - 2),
                       *(position + 2) - *(position - 1));
        } else {
            glNormal3f(*(position + 3) - *position,
                       *(position + 4) - *(position + 1),
                       *(position + 5) - *(position + 2));
        }
        glVertex3fv(xyz);

        position += 3;
        color += 4;
    }
    glEnd();
}

qreal GlView::cameraZ() const
{
    return m_cameraZ;
}

qreal GlView::modelRotationX() const
{
    return m_modelRotationX;
}

qreal GlView::modelRotationY() const
{
    return m_modelRotationY;
}

void GlView::setCameraZ(qreal z)
{
    double newCameraZ = qRound(z * 10.0) / 10.0;
    if (newCameraZ > -0.9)
        newCameraZ = -0.9;
    else if (newCameraZ < -100.0)
        newCameraZ = -100.0;

    if (qFuzzyCompare(m_cameraZ, newCameraZ))
        return;

    m_cameraZ = newCameraZ;
    emit cameraZChanged(m_cameraZ);
    update();
}

void GlView::setModelRotationX(qreal angle)
{
    m_modelRotationX = angle;
    update();
}

void GlView::setModelRotationY(qreal angle)
{
    m_modelRotationY = angle;
    update();
}

void GlView::initializeGL()
{
    context()->makeCurrent();

    glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_LINE_SMOOTH);
    glEnable(GL_MULTISAMPLE);
    glLineWidth(2.0f);
    glColorMaterial(GL_FRONT, GL_AMBIENT_AND_DIFFUSE);
}

void GlView::resizeGL(int width, int height)
{
    context()->makeCurrent();

    glViewport(0, 0, width, height);
}

void GlView::paintGL()
{
    if (!isVisible())
        return;

    context()->makeCurrent();

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    float intensity[] = { 1.0f, 1.0f, 1.0f, 1.0f };
    glLightfv(GL_LIGHT0, GL_DIFFUSE, intensity);
    glLightfv(GL_LIGHT0, GL_SPECULAR, intensity);
    glEnable(GL_LIGHT0);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glTranslated(0.0, 0.0, m_cameraZ);
    glRotated(m_modelRotationX, 1.0, 0.0, 0.0);
    glRotated(m_modelRotationY, 0.0, 1.0, 0.0);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    double ratio = double(width()) / double(height());
    gluPerspective(25.0, ratio, 0.1, 1000.0);

    glEnable(GL_LIGHTING);
    glEnable(GL_COLOR_MATERIAL);

    if (m_state.modelId() && m_state.isEnabled(ViewState::ModelList) && !m_pressed) {
        glCallList(m_state.modelId());
    }

    glDisable(GL_LIGHTING);

    if (m_state.normalMapId() && m_state.isEnabled(ViewState::NormalMapList) && !m_pressed) {
        glClear(GL_DEPTH_BUFFER_BIT);
        glCallList(m_state.normalMapId());
    }

    if (m_state.wireFrameId() && (m_state.isEnabled(ViewState::WireFrameList) || m_pressed)) {
        glCallList(m_state.wireFrameId());
    }
}

void GlView::mousePressEvent(QMouseEvent *event)
{
    setCursor(Qt::ClosedHandCursor);
    m_lastPoint = event->pos();
    m_pressed = true;
}

void GlView::mouseReleaseEvent(QMouseEvent *event)
{
    setCursor(Qt::ArrowCursor);
    applyRotation(m_lastPoint, event->pos());
    m_lastPoint = QPointF();
    m_pressed = false;
}

void GlView::mouseMoveEvent(QMouseEvent *event)
{
    if (!m_pressed)
        return;

    applyRotation(m_lastPoint, event->pos());
    m_lastPoint = event->pos();
}

void GlView::wheelEvent(QWheelEvent *event)
{
    int delta = event->delta();
    double factor = 1.0 + double(std::abs(delta)) / 2000.0;
    if (delta > 0)
        factor = 1.0 / factor;
    qreal z = m_cameraZ * factor;
    setCameraZ(z);
}

void GlView::applyRotation(const QPointF &start, const QPointF &current)
{
    QPointF delta = current - start;
    m_modelRotationX += 180.0 * delta.y() / height();
    m_modelRotationY += 180.0 * delta.x() / width();
    update();
}
